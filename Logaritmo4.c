#include <stdio.h>
#include <getopt.h>
#include <stdlib.h>

double C1 = 1.000000000000000000;
double C2 = 0.333333333333333310;
double C3 = 0.200000000000000010;
double C4 = 0.142857142857142850;
double C5 = 0.111111111111111100;
double C6 = 0.090909090909090912;
double C7 = 0.076923076923076927;
double C8 = 0.066666666666666666;
double C9 = 0.058823529411764705;
double C10 = 0.052631578947368418;

double logaritmo (double x) {
	double y, y2, y3, y6, y5, y8, a, b, c;
	 y = ((x-1)/(x+1));
	 //A = C1*y;
	 y3 = y*y*y;
	 y2 = y*y;
	 //B = C2*y3;
	 y5= y2*y3;
	 //C = C3*y5;
	 y6 = y3 *y3;
	 //D = C4*y6*y;
	 //E = C5*y6*y3;
	 //F = C6*y5*y6;
	 y8= y3*y5;
	 //G = C7*y8*y5;
	 //H = C8*y8*y6*y; 
	 //I = C9*y8*y8*y;
	 //J = C10*y8*y8*y3;
	 a = (C1*y+y3*(C2+y*(C3*y+y3*C4)));
	 b = (y6*y3*(C5+y2*(C7*y2+C6)));
	 c = (y8*y6*(C9*y+y2*(y*C8+y3*C10)));
	return 2*(a+b+c);
} 

int main (int argc, char **argv) {
  int opcion, n, i, k;
  char *numero = NULL, *iteracion = NULL;
  while ((opcion = getopt (argc, argv, "n: i:")) != -1) {
    switch (opcion) {
      case 'n' : 
          numero = optarg;
		  n = atoi(numero);
          break;
			  
      case 'i' : 
          iteracion = optarg;
		  i = atoi(iteracion);
          break;
			  
      default :
          printf("\t\t*****Algo salio mal, lo lamentamos, comunicate con nuestro equipo y te solucionaremos el problema.*****\n");
	  }
  }
  if (n > 0 && i > 0){
  	for (k = 0; k < i; ++k){
    	logaritmo(n);
  	}
  	printf("\n\nEl logaritmo de %.0f es:", n);
  	printf("\t%.5f \n\n", logaritmo(n));
  }
  else {
  	printf("Algunos de los parametros ingresados no son correctos, revise el manual de usuario");
  }
  return 0;
}