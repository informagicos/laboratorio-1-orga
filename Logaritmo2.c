#include <stdio.h>
#include <getopt.h>
#include <stdlib.h>

double C1 = 1.000000000000000000;
double C2 = 0.333333333333333310;
double C3 = 0.200000000000000010;
double C4 = 0.142857142857142850;
double C5 = 0.111111111111111100;
double C6 = 0.090909090909090912;
double C7 = 0.076923076923076927;
double C8 = 0.066666666666666666;
double C9 = 0.058823529411764705;
double C10 = 0.052631578947368418;

double logaritmo (double x) {
    double y, A, B, C, D, E, F, G, H, I, J, acum;
    y = ((x-1)/(x+1));
    A = C1*y;
    acum = y*y*y;
    B = C2*acum;
    acum = acum *y*y;
    C = C3*acum;
    acum = acum *y*y;
    D = C4*acum;
    acum = acum *y*y;
    E = C5*acum;
    acum = acum *y*y;
    F = C6*acum;
    acum = acum *y*y;
    G = C7*acum;
    acum = acum *y*y;
    H = C8*acum;
    acum = acum *y*y;
    I = C9*acum;
    acum = acum *y*y;
    J = C10*acum;
    return 2*(A+B+C+D+E+F+G+H+I+J);
}

int main (int argc, char **argv) {
  int opcion, k;
  double n, i;
  char *numero = NULL, *iteracion = NULL;
  while ((opcion = getopt (argc, argv, "n: i:")) != -1) {
    switch (opcion) {
      case 'n' : 
          numero = optarg;
		  n = atoi(numero);
          break;
			  
      case 'i' : 
          iteracion = optarg;
		  i = atoi(iteracion);
          break;
			  
      default :
          printf("\t\t*****ALGO SALIO MAL, LO LAMENTAMOS*****\n");
	  }
  }
  if (n > 0 && i > 0){
  	for (k = 0; k < i; ++k){
    	logaritmo(n);
  	}
  	printf("\n\nEl logaritmo de %.6f es: %.6f", n, logaritmo(n));
  }
  else if(n < 0 || n == 0){
  	printf("El numero ingresado a calcular no es correcto\n");
  }
  else {
  	printf("Alguno de los parametros ingresados no es correcto, revise por favor el manual de usuario");
  }
  return 0;
}
